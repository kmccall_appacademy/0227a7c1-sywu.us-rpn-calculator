require 'byebug'
class RPNCalculator
  #TODO
  attr_accessor  :calculator
  def initialize
    @stack = []
  end
  
  def push(num)
    @stack.push(num)
  end
  
  def plus
    self.check
    pop = @stack.pop
    @stack[-1] += pop
  end
  
  def minus
    self.check
    pop = @stack.pop
    @stack[-1] -= pop
  end
  
  def times
    self.check
    pop = @stack.pop
    @stack[-1] *= pop
  end
  
  def divide
    self.check
    pop = @stack.pop
    @stack[-1] /= pop.to_f
  end
  
  def value
    @stack[-1]
  end
  
  def check
    if @stack.length < 2
      raise "calculator is empty"
    end
  end
  
  def tokens(str)
    operations = "+-*/"
    str.split.map! do |el|
      if operations.include?(el)
        el.to_sym
      else
        el.to_i
      end
    end
  end
  
  def evaluate(str)
    self.tokens(str).each do |el|
      if el == :+
        self.plus
      elsif el == :-
        self.minus
      elsif el == :*
        self.times
      elsif el == :/
        self.divide
      else
        self.push(el)
      end
    end
    self.value
  end
      
end
